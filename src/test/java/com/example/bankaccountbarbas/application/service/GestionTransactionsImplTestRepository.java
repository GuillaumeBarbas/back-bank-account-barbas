package com.example.bankaccountbarbas.application.service;

import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import com.example.bankaccountbarbas.infrastructure.persistence.mappers.CompteBancaireMapper;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.CompteBancaireJpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.example.bankaccountbarbas.*")
public class GestionTransactionsImplTestRepository {

    private final static String uuidCompteBancaire = "TESTf62-11ed-be56-0242ac120002";
    private final static BigDecimal soldeBase = new BigDecimal(50);
    @Autowired
    private GestionTransactionRepository gestionTransaction;
    @Autowired
    private GestionCompteBancaire gestionCompteBancaire;
    @Autowired
    private CompteBancaireJpaRepository compteBancaireJpaRepository;
    @Autowired
    private CompteBancaireMapper compteBancaireMapper;

    @BeforeEach
    void setUp() {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setIdCompteBancaire(uuidCompteBancaire);
        compteBancaire.setSolde(soldeBase);
        compteBancaire.setDateCreation(LocalDate.now());
        compteBancaire.setTitulaire("Guillaume Barbas");

        compteBancaireJpaRepository.save(compteBancaireMapper.mapToJpaEntity(compteBancaire));
    }

    @Test
    @DisplayName("Verification de l'ajout de transaction lors d'un crédit ou débit et du bon type des transactions")
    void ajoutTransaction() {
        gestionCompteBancaire.debit(uuidCompteBancaire, new BigDecimal("129.35"));
        gestionCompteBancaire.credit(uuidCompteBancaire, new BigDecimal("30"));
        gestionCompteBancaire.credit(uuidCompteBancaire, new BigDecimal("70"));
        gestionCompteBancaire.credit(uuidCompteBancaire, new BigDecimal("30"));

        int page = 0;
        int taillePage = 10;
        Pageable pageable = PageRequest.of(page, taillePage);

        Page<Transaction> transactionsPage = gestionTransaction.recupererTransactions(uuidCompteBancaire, pageable);

        List<Transaction> transactions = transactionsPage.getContent();
        assertEquals(4, transactions.size());

        int compteurCredit = 0;
        int compteurDebit = 0;

        for (Transaction transaction : transactions) {
            if ("crédit".equals(transaction.getType())) {
                compteurCredit++;
            } else if ("débit".equals(transaction.getType())) {
                compteurDebit++;
            }
        }

        assertEquals(3, compteurCredit);
        assertEquals(1, compteurDebit);
    }

    @Test
    @DisplayName("Sauvegarde d'une transaction par le service")
        //TODO Ajouter Enum pour credit/debit et tout les types selon les cas
    void sauvegardeTransaction() {
        gestionTransaction.sauvegarder(uuidCompteBancaire, soldeBase, "crédit");
        int page = 0;
        int taillePage = 10;

        Pageable pageable = PageRequest.of(page, taillePage);


        Page<Transaction> transactionsPage = gestionTransaction.recupererTransactions(uuidCompteBancaire, pageable);

        List<Transaction> transactions = transactionsPage.getContent();
        assertEquals(1, transactions.size());
    }

}
