package com.example.bankaccountbarbas.application.service;


import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import com.example.bankaccountbarbas.infrastructure.persistence.mappers.CompteBancaireMapper;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.CompteBancaireJpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.example.bankaccountbarbas.*")
public class GetCompteBancaireImplTest {

    private final static String uuidCompteBancaire = "TESTf62-11ed-be56-0242ac120002";
    @Autowired
    private GestionCompteBancaire gestionCompteBancaire;
    @Autowired
    private CompteBancaireJpaRepository compteBancaireJpaRepository;
    @Autowired
    private CompteBancaireMapper compteBancaireMapper;

    @BeforeEach
    void setUp() {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setIdCompteBancaire(uuidCompteBancaire);
        compteBancaire.setSolde(new BigDecimal(50));
        compteBancaire.setDateCreation(LocalDate.now());
        compteBancaire.setTitulaire("Guillaume Barbas");

        compteBancaireJpaRepository.save(compteBancaireMapper.mapToJpaEntity(compteBancaire));
    }

    @Test
    @DisplayName("Test de retour de valeur")
    void getCompteBancaire() {
        CompteBancaire compteBancaire = gestionCompteBancaire.getCompteBancaire(uuidCompteBancaire);
        assertEquals("Guillaume Barbas", compteBancaire.getTitulaire());
    }


}
