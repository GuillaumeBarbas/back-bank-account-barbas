package com.example.bankaccountbarbas.infastructure;

import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import com.example.bankaccountbarbas.infrastructure.persistence.mappers.CompteBancaireMapper;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.CompteBancaireJpaRepository;
import org.junit.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.example.bankaccountbarbas.*")
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GestionCompteBancaire gestionCompteBancaire;
    @Autowired
    private GestionCompteBancaire getCompteBancaire;
    @Autowired
    private CompteBancaireJpaRepository compteBancaireJpaRepository;
    @Autowired
    private CompteBancaireMapper compteBancaireMapper;
    @Autowired
    private GestionTransactionRepository gestionTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test de l'api sur l'endpoint d'un crédit ")
    public void creditRequest() throws Exception {
        String requestBody = "{\"id\":\"6f1e94dc-9b0d-4d71-b8f7-2dd632920cf3\",\"montant\":15.00}";
        mockMvc.perform(patch("/compte-bancaire/credit")
                        .contentType("application/json")
                        .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test de l'api sur l'endpoint d'un débit")
    public void debitRequest() throws Exception {
        String requestBody = "{\"id\":\"6f1e94dc-9b0d-4d71-b8f7-2dd632920cf3\",\"montant\":15.00}";
        mockMvc.perform(patch("/compte-bancaire/debit")
                        .contentType("application/json")
                        .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test de l'api pour récuperer les transactions d'un compte bancaire")
    public void getTransactions() throws Exception {

        String requestBody = "{\"id\":\"6f1e94dc-9b0d-4d71-b8f7-2dd632920cf3\",\"montant\":60,\"taillePage\":2,\"page\":0}";        mockMvc.perform(get("/compte-bancaire/transactions")
                        .contentType("application/json")
                        .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].idCompteBancaire").value("6f1e94dc-9b0d-4d71-b8f7-2dd632920cf3"))
                .andExpect(jsonPath("$[1].montant").value(15.00))
                .andExpect(jsonPath("$[1].type").value("débit"));
    }


}

