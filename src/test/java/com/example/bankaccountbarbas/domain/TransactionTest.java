package com.example.bankaccountbarbas.domain;

import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.example.bankaccountbarbas.*")
public class TransactionTest {

    @Test
    @DisplayName("Test de création de transaction")
    void creationTransaction() {
        Transaction transaction = new Transaction();
        Assertions.assertNotNull(transaction);
    }
}
