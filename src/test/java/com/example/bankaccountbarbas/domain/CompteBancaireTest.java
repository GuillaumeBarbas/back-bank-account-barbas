package com.example.bankaccountbarbas.domain;

import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@ComponentScan(basePackages = "com.example.bankaccountbarbas.*")
public class CompteBancaireTest {

    @Test
    @DisplayName("Test des setter pour vérifier la non régression suite à une modification de l'objet")
    void creerCompteBancaire() {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setIdCompteBancaire(UUID.randomUUID().toString());
        compteBancaire.setSolde(new BigDecimal(50));
        compteBancaire.setDateCreation(LocalDate.now());
        compteBancaire.setTitulaire("Guillaume Barbas");
        assertNotNull(compteBancaire);
    }

    @Test
    @DisplayName("Test compte non négatif")
    void compteNegatif() {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setIdCompteBancaire(UUID.randomUUID().toString());
        compteBancaire.setSolde(new BigDecimal(50));
        compteBancaire.setDateCreation(LocalDate.now());
        compteBancaire.setTitulaire("Guillaume Barbas");

        compteBancaire.retirerSolde(new BigDecimal(60));
        assertTrue(compteBancaire.getSolde().compareTo(BigDecimal.ZERO) < 0);
    }

    @Test
    @DisplayName("Test fonctionalité ajouter du solde")
    void ajoutSolde() {
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setIdCompteBancaire(UUID.randomUUID().toString());
        BigDecimal soldeAverifier = new BigDecimal(50);
        compteBancaire.setSolde(soldeAverifier);
        compteBancaire.setDateCreation(LocalDate.now());
        compteBancaire.setTitulaire("Guillaume Barbas");

        compteBancaire.ajouterSolde(new BigDecimal(60));
        assertTrue(compteBancaire.getSolde().compareTo(soldeAverifier) >= 1);
    }
}

