package com.example.bankaccountbarbas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.example.bankaccountbarbas")
public class BankaccountBarbasApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankaccountBarbasApplication.class, args);
    }

}
