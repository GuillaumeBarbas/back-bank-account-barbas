package com.example.bankaccountbarbas.domain.CompteBancaire;

import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.port.out.GestionCompteBancaireRepository;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
@RequiredArgsConstructor
public class GestionCompteBancaireImpl implements GestionCompteBancaire {

    private final GestionCompteBancaireRepository compteBancairePersistenceAdaptater;

    private final GestionTransactionRepository transactionPersistanceAdaptater;

    private final String credit = "crédit";

    private final String debit = "débit";

    @Override
    public void credit(String idCompteBancaire, BigDecimal montant) {
        log.info("Crédit à faire {},{}", idCompteBancaire, montant);
        if (idCompteBancaire == null || montant == null) {
            throw new IllegalArgumentException("L'id du compte bancaire ou le montant ne peut pas être null");
        }
        if (idCompteBancaire.isEmpty() || montant.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("L'id du compte bancaire ne peut pas être vide et le montant ne peut pas être zéro ou négatif");
        }
        CompteBancaire compteBancaire = compteBancairePersistenceAdaptater.getCompteBancaire(idCompteBancaire);
        compteBancaire.ajouterSolde(montant);
        compteBancairePersistenceAdaptater.creerCompteBancaire(compteBancaire);
        transactionPersistanceAdaptater.sauvegarder(idCompteBancaire, montant, credit);
        log.info("Crédit éffectué pour le compte {} avec le montant : {}", idCompteBancaire, montant);
    }

    @Override
    public void debit(String idCompteBancaire, BigDecimal montant) {
        if (idCompteBancaire == null || montant == null) {
            throw new IllegalArgumentException("L'id du compte bancaire ou le montant ne peut pas être null");
        }
        if (idCompteBancaire.isEmpty() || montant.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("L'id du compte bancaire ne peut pas être vide et le montant ne peut pas être zéro ou négatif");
        }
        CompteBancaire compteBancaire = compteBancairePersistenceAdaptater.getCompteBancaire(idCompteBancaire);
        compteBancaire.retirerSolde(montant);
        compteBancairePersistenceAdaptater.creerCompteBancaire(compteBancaire);
        transactionPersistanceAdaptater.sauvegarder(idCompteBancaire, montant, debit);
        log.info("Débit éffectué pour le compte {} avec le montant : {}", idCompteBancaire, montant);
    }

    @Override
    public CompteBancaire getCompteBancaire(String idCompteBancaire) {
        if (idCompteBancaire == null) {
            throw new IllegalArgumentException("L'id du compte bancaire ne peut pas être null");
        }
        return compteBancairePersistenceAdaptater.getCompteBancaire(idCompteBancaire);
    }

    @Override
    public BigDecimal voirSolde(String idCompteBancaire) {
        if (idCompteBancaire == null) {
            throw new IllegalArgumentException("L'id du compte bancaire ne peut pas être null");
        }
        return compteBancairePersistenceAdaptater.getCompteBancaire(idCompteBancaire).getSolde();
    }

}
