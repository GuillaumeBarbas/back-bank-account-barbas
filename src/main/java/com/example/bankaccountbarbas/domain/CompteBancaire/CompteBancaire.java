package com.example.bankaccountbarbas.domain.CompteBancaire;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Classe représentant un compte bancaire.
 */
@Data
public class CompteBancaire {

    private String idCompteBancaire;

    private BigDecimal solde;

    private String titulaire;

    private LocalDate dateCreation;

    /**
     * Fonction permettant le crédit d'un compte banciare
     *
     * @param montant Le montant ajouter au solde
     */
    public void ajouterSolde(BigDecimal montant) {
        if (montant.compareTo(BigDecimal.ZERO) > 0) {
            this.solde = this.solde.add(montant);
        } else {
            throw new IllegalArgumentException("Le montant ne peut pas être zéro ou négatif");
        }
    }

    /**
     * Fonction permettant le débit d'un compte banciare
     *
     * @param montant Le montant soustrait au solde
     */
    public void retirerSolde(BigDecimal montant) {
        if (montant.compareTo(BigDecimal.ZERO) > 0) {
            this.solde = this.solde.subtract(montant);
        } else {
            throw new IllegalArgumentException("Le montant ne peut pas être zéro ou négatif");
        }
    }

}
