package com.example.bankaccountbarbas.domain.port.out;

import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface GestionTransactionRepository {

    /**
     * Méthode de sauvegarde d'une transaction : persiste une transaction dans le système de persistance.
     *
     * @param idCompteBancaire l'id du compte bancaire de la transaction
     * @param montant          le montant lié à la transaction
     * @param type             un crédit,débit ou autre
     */
    void sauvegarder(String idCompteBancaire, BigDecimal montant, String type);

    /**
     * Recupere les transactions d'un compte selon un id
     *
     * @param idCompteBancaire Le compte bancaire pour obtenir ses transactions
     * @return Une liste de transaction d'un compte bancaire
     */
    Page<Transaction> recupererTransactions(String idCompteBancaire, Pageable pageable);
}
