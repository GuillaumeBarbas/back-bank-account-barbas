package com.example.bankaccountbarbas.domain.port.in;

import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import com.example.bankaccountbarbas.infrastructure.persistence.model.TransactionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GestionTransaction {

    /**
     * Recupere les transactions d'un compte selon un id
     *
     * @param idCompteBancaire Le compte bancaire pour obtenir ses transactions
     * @return Une liste de transaction d'un compte bancaire
     */
    List<Transaction> recupererTransactions(String idCompteBancaire, Pageable pageable);
}
