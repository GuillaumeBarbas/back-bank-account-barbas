package com.example.bankaccountbarbas.domain.port.out;

import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;

import java.math.BigDecimal;

public interface GestionCompteBancaireRepository {

    /**
     * Récupère un compte bancaire selon son id
     *
     * @param idCompteBancaire
     * @return le compte banquaire associé à l'id
     */
    CompteBancaire getCompteBancaire(String idCompteBancaire);

    //TODO ne pas écraser mais simplement d'update le compte bancaire en question
    /**
     * Permet d'écraser un compte bancaire pour le modifier
     *
     * @param compteBancaire le compte bancaire à sauvegarder
     */
    void creerCompteBancaire(CompteBancaire compteBancaire);

}
