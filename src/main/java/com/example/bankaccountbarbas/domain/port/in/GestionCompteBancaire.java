package com.example.bankaccountbarbas.domain.port.in;

import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;

import java.math.BigDecimal;

/**
 * Interface permettant de gérer les comptes bancaires vers les ports exterieurs
 */
public interface GestionCompteBancaire {

    /**
     * Méthode permettant l'ajout d'un montant sur un solde
     *
     * @param idCompteBancaire le compte bancaire auquel on veut rajouter
     * @param montant          montant ajouter voulu
     */
    void credit(String idCompteBancaire, BigDecimal montant);

    /**
     * Méthode permettant le retrait d'un montant sur un solde
     *
     * @param idCompteBancaire le compte bancaire auquel on veut retirer
     * @param montant          montant ajouter voulu
     */
    void debit(String idCompteBancaire, BigDecimal montant);

    /**
     * Récupère un compte bancaire selon son id
     *
     * @param idCompteBancaire
     * @return le compte banquaire associé à l'id
     */
    CompteBancaire getCompteBancaire(String idCompteBancaire);

    /**
     * Méthode pour voir son solde
     *
     * @param idCompteBancaire du compte de la personnne
     * @return le solde
     */
    BigDecimal voirSolde(String idCompteBancaire);

}
