package com.example.bankaccountbarbas.domain.Transaction;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Classe représentant une transaction.
 */
@Data
@AllArgsConstructor
public class Transaction {

    private String idTransaction;

    private String idCompteBancaire;

    private BigDecimal montant;

    private String type;

    private LocalDate date;

    public Transaction() {
    }
}
