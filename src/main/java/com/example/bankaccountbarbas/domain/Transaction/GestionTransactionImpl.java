package com.example.bankaccountbarbas.domain.Transaction;

import com.example.bankaccountbarbas.domain.port.in.GestionTransaction;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import com.example.bankaccountbarbas.infrastructure.persistence.model.TransactionEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class GestionTransactionImpl implements GestionTransaction {

    private final GestionTransactionRepository gestionTransactionRepository;

    @Override
    public List<Transaction> recupererTransactions(String idCompteBancaire, Pageable pageable) {
        log.info("Récupération de la transaction du compte : {}", idCompteBancaire);
        if (idCompteBancaire == null) {
            throw new IllegalArgumentException("L'id du compte bancaire ne peut pas être null");
        }
        Page<Transaction> transactionsPage = gestionTransactionRepository.recupererTransactions(idCompteBancaire, pageable);
        return transactionsPage.getContent();
    }
}
