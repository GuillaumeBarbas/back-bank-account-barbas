package com.example.bankaccountbarbas.infrastructure.persistence.adaptaters.out;

import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import com.example.bankaccountbarbas.infrastructure.persistence.mappers.TransactionMapper;
import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.CompteBancaireJpaRepository;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.TransactionJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;


//TODO Je me suis trompé dans l'architecture je devrais avoir un interface qui implement les adaptater et non reutilisé ceux du user-side, j'ai déjà envoyé le lien du gitlab pour une review. Veuillez m'excuser de cette erreur.
@RequiredArgsConstructor
@Component
public class TransactionPersistanceAdaptater implements GestionTransactionRepository {

    private final TransactionJpaRepository transactionJpaRepository;

    private final CompteBancaireJpaRepository compteBancaireRepository;

    private final TransactionMapper transactionMapper;

    @Override
    public void sauvegarder(String idCompteBancaire, BigDecimal montant, String type) {
        //TODO Mettre le type UUID dans l'entity
        String uuid = UUID.randomUUID().toString();
        Transaction transaction = new Transaction(uuid, idCompteBancaire, montant, type, LocalDate.now());
        transactionJpaRepository.save(TransactionMapper.INSTANCE.transactionToTransactionEntity(transaction));
    }

    @Override
    public Page<Transaction> recupererTransactions(String idCompteBancaire, Pageable page) {
        CompteBancaireEntity entity = compteBancaireRepository.findById(idCompteBancaire).orElse(null);
        if (entity == null) {
            return Page.empty();
        }


        return TransactionMapper.INSTANCE.pageTransactionEntityToPageTransaction(transactionJpaRepository.findAllIdByCompteBancaire(entity, page));
    }

}
