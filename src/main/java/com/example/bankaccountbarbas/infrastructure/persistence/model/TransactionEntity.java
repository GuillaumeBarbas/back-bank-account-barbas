package com.example.bankaccountbarbas.infrastructure.persistence.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Classe représentant une transaction.
 */
@Entity(name = "transaction_compte_bancaire")
@Data
@Getter
@Setter
public class TransactionEntity {

    @Id
    private String idTransaction;

    //TODO Ajouter une relation OneToMany

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_compte_bancaire", nullable = false)
    private CompteBancaireEntity compteBancaire;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "montant", nullable = false)
    private BigDecimal montant;

    @Column(name = "date", nullable = false)
    private LocalDate date;

}
