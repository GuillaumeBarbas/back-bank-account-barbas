package com.example.bankaccountbarbas.infrastructure.persistence.adaptaters.out;

import com.example.bankaccountbarbas.domain.port.out.GestionCompteBancaireRepository;
import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import com.example.bankaccountbarbas.infrastructure.persistence.mappers.CompteBancaireMapper;
import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import com.example.bankaccountbarbas.infrastructure.persistence.repository.CompteBancaireJpaRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

//TODO Je me suis trompé dans l'architecture je devrais avoir un interface qui implement les adaptater et non reutilisé ceux du user-side, j'ai déjà envoyé le lien du gitlab pour une review. Veuillez m'excuser de cette erreur.
@AllArgsConstructor
@Component
public class CompteBancairePersistenceAdaptater implements GestionCompteBancaireRepository {

    @Autowired
    private final CompteBancaireJpaRepository compteBancaireJpaRepository;

    @Autowired
    private final CompteBancaireMapper compteBancaireMapper;


    @Override
    public CompteBancaire getCompteBancaire(String idCompteBancaire) {
        CompteBancaireEntity e = compteBancaireJpaRepository.findById(idCompteBancaire).orElse(null);
        String test =" tes";
        return compteBancaireMapper.entityToDTO(compteBancaireJpaRepository.findById(idCompteBancaire));
    }

    @Override
    public void creerCompteBancaire(CompteBancaire compteBancaire) {
        compteBancaireJpaRepository.save(compteBancaireMapper.mapToJpaEntity(compteBancaire));
    }

}
