package com.example.bankaccountbarbas.infrastructure.persistence.mappers;

import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Optional;

/**
 * Mapper pour gérer la transformation entre JPA et Domain
 */
@Mapper(componentModel = "spring")
public interface CompteBancaireMapper {

    CompteBancaireMapper INSTANCE = Mappers.getMapper(CompteBancaireMapper.class);

    /**
     * Mappe un objet compte bancaire en entity pour la persistence
     *
     * @param compteBancaire compte bancaire à transformer
     * @return l'entity mappée
     */
    @Mapping(target = "idCompteBancaire", source = "compteBancaire.idCompteBancaire")
    @Mapping(target = "titulaire", source = "compteBancaire.titulaire")
    @Mapping(target = "solde", source = "compteBancaire.solde")
    @Mapping(target = "dateCreation", source = "compteBancaire.dateCreation")
    CompteBancaireEntity mapToJpaEntity(CompteBancaire compteBancaire);

    /**
     * Mappe un objet compte bancaire en entity pour la persistence
     *
     * @param optionalCompteBancaireEntity compte bancaire récupéré de la base de données à transformer
     * @return CompteBancaire depuis une entity
     */
    default CompteBancaire entityToDTO(Optional<CompteBancaireEntity> optionalCompteBancaireEntity) {
        return optionalCompteBancaireEntity.map(compteBancaireEntity -> {
            CompteBancaire compteBancaire = new CompteBancaire();
            compteBancaire.setIdCompteBancaire(compteBancaireEntity.getIdCompteBancaire());
            compteBancaire.setSolde(compteBancaireEntity.getSolde());
            compteBancaire.setTitulaire(compteBancaireEntity.getTitulaire());
            compteBancaire.setDateCreation(compteBancaireEntity.getDateCreation());
            return compteBancaire;
        }).orElse(null);
    }
}
