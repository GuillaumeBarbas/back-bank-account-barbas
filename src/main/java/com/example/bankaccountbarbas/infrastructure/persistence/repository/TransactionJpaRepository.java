package com.example.bankaccountbarbas.infrastructure.persistence.repository;

import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import com.example.bankaccountbarbas.infrastructure.persistence.model.TransactionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

@Repository
public interface TransactionJpaRepository extends JpaRepository<TransactionEntity, String> {
    Page<TransactionEntity> findAllIdByCompteBancaire(CompteBancaireEntity compteBancaire, Pageable pageable);
}


