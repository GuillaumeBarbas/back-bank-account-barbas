package com.example.bankaccountbarbas.infrastructure.persistence.repository;

import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CompteBancaireJpaRepository extends JpaRepository<CompteBancaireEntity, String> {

}
