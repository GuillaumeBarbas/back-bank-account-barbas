package com.example.bankaccountbarbas.infrastructure.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant une entité de compte bancaire.
 */
@Entity(name = "compte_bancaire")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompteBancaireEntity {
    @Id
    private String idCompteBancaire;

    @Column(name = "solde", nullable = false)
    private BigDecimal solde;

    @Column(name = "titulaire", nullable = false)
    private String titulaire;

    @Column(name = "dateCreation", nullable = false)
    private LocalDate dateCreation;

    @OneToMany(mappedBy = "compteBancaire")
    private List<TransactionEntity> transactionsList = new ArrayList<>();


}
