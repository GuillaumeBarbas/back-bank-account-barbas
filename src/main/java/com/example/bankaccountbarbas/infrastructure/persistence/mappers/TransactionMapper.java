package com.example.bankaccountbarbas.infrastructure.persistence.mappers;

import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import com.example.bankaccountbarbas.infrastructure.persistence.model.CompteBancaireEntity;
import com.example.bankaccountbarbas.infrastructure.persistence.model.TransactionEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    TransactionMapper INSTANCE = Mappers.getMapper(TransactionMapper.class);

    @Mapping(source = "idCompteBancaire", target = "compteBancaire.idCompteBancaire")
    TransactionEntity transactionToTransactionEntity(Transaction transaction);

    default String map(CompteBancaireEntity value) {
        return value.getIdCompteBancaire();
    }

    @Mapping(source = "idTransaction", target = "idTransaction")
    @Mapping(source = "montant", target = "montant")
    @Mapping(source = "type", target = "type")
    @Mapping(source = "date", target = "date")
    @Mapping(source = "compteBancaire", target = "idCompteBancaire")
    Transaction transactionEntityToTransaction(TransactionEntity transactionEntity);

    default Page<Transaction> pageTransactionEntityToPageTransaction(Page<TransactionEntity> transactionEntityPage) {
        return transactionEntityPage.map(this::transactionEntityToTransaction);
    }
}
