package com.example.bankaccountbarbas.application.controller;

import com.example.bankaccountbarbas.application.controller.model.Request;
import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.port.in.GestionTransaction;
import com.example.bankaccountbarbas.domain.CompteBancaire.CompteBancaire;
import com.example.bankaccountbarbas.domain.Transaction.Transaction;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class Controller {

    private final GestionCompteBancaire gestionCompteBancaire;

    private final GestionTransaction gestionTransaction;

    //TODO Changer compteBancaireRequest et ne pas le mettre partout

    /**
     * Récupère un compte bancaire selon l'id
     *
     * @param compteBancaireRequest Classe permettant de gerer les requête on utilisera que l'id
     * @return le compte bancaire trouvé
     */
    @GetMapping("/compte-bancaire")
    public ResponseEntity<CompteBancaire> getCompteBancaireById(@RequestBody Request compteBancaireRequest) {
        return ResponseEntity.ok().body(gestionCompteBancaire.getCompteBancaire(compteBancaireRequest.getId()));
    }

    /**
     * Endpoint pour pouvoir récupérer son solde
     *
     * @param compteBancaireRequest id du compte
     * @return le solde du compte
     */
    @GetMapping("/compte-bancaire/solde")
    public ResponseEntity<BigDecimal> voirSolde(@RequestBody Request compteBancaireRequest) {
        return ResponseEntity.ok().body(gestionCompteBancaire.voirSolde(compteBancaireRequest.getId()));
    }

    /**
     * Endpoint pour récuperer les transactions d'un compte bancaire
     *
     * @param transactionRequest
     * @return La liste des transactions
     */
    @GetMapping("/compte-bancaire/transactions")
    public ResponseEntity<List<Transaction>> recupererTransactions(@RequestBody Request transactionRequest) {
        int page = transactionRequest.getPage(); // Numéro de la page
        int taillePage = transactionRequest.getTaillePage(); // Taille de la page

        PageRequest pageable = PageRequest.of(page, taillePage);
        List<Transaction> transactions = gestionTransaction.recupererTransactions(transactionRequest.getId(), pageable);

        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    /**
     * Requete permettant de crédité un compte selon un id et un montant
     *
     * @param compteBancaireRequest id du compte à créditer et un montant
     * @return ResponseEntity ok si le compte à bien était crédité
     */
    @PatchMapping("compte-bancaire/credit")
    public ResponseEntity credit(@RequestBody Request compteBancaireRequest) {
        BigDecimal decimal = compteBancaireRequest.getMontant();
        gestionCompteBancaire.credit(compteBancaireRequest.getId(), decimal);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Requete permettant de débité un compte selon un id et un montant
     *
     * @param compteBancaireRequest id du compte à débiter et un montant
     * @return ResponseEntity ok si le compte à bien était débité
     */
    @PatchMapping("compte-bancaire/debit")
    public ResponseEntity<CompteBancaire> debit(@RequestBody Request compteBancaireRequest) {
        BigDecimal decimal = compteBancaireRequest.getMontant();
        gestionCompteBancaire.debit(compteBancaireRequest.getId(), decimal);
        return new ResponseEntity(HttpStatus.OK);
    }

}
