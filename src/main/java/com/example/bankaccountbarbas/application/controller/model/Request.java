package com.example.bankaccountbarbas.application.controller.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Classe representant le corps d'une requête pour des opérations liées à un compte bancaire
 */
@Getter
@Setter
public class Request {
    private String id;
    private BigDecimal montant;

    private int taillePage;
    private int page;
}