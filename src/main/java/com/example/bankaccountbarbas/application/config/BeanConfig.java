package com.example.bankaccountbarbas.application.config;

import com.example.bankaccountbarbas.domain.port.in.GestionCompteBancaire;
import com.example.bankaccountbarbas.domain.port.in.GestionTransaction;
import com.example.bankaccountbarbas.domain.port.out.GestionCompteBancaireRepository;
import com.example.bankaccountbarbas.domain.port.out.GestionTransactionRepository;
import com.example.bankaccountbarbas.domain.CompteBancaire.GestionCompteBancaireImpl;
import com.example.bankaccountbarbas.domain.Transaction.GestionTransactionImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    GestionTransaction gestionTransaction(GestionTransactionRepository repo){
        return new GestionTransactionImpl(repo);
    }

    @Bean
    GestionCompteBancaire gestionCompteBancaire(GestionCompteBancaireRepository repoCompteBancaire,GestionTransactionRepository repository){
        return new GestionCompteBancaireImpl(repoCompteBancaire,repository);
    }
}
