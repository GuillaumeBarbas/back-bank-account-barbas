# Kata-Exaltit-Barbas-Guillaume

# Présentation

Bienvenue sur mon kata du bank account. Je tenais tout d'abord à vous remercier pour la soirée de recrutement qui était
très intéressante, j'ai pu apprendre pleins de choses. Le projet fonctionne en spring boot 2.7.10, je voulais utiliser
une version plus récente, mais intelliJ en a décider autrement. Vous n'avez pas besoin d'outil externe pour lancer ce
projet tout est fait avec une base H2, un postman suffira pour tester le projet et ses fonctionnalités. Le projet n'est
certainement pas parfait, mais j'espère qu'il conviendra néanmoins à vos attentes. Il y a une liste de TODO qui sont des
points à améliorer dont j'ai conscience, mais j'ai voulu rester sur quelque chose de plus simple et fonctionnel.

## Fonctionalités

Il y a 6 endpoint à tester :

- récupérer un compte bancaire
- récupérer le solde d'un compte bancaire
- créditer un compte
- débiter un compte
- afficher les transactions d'un compte bancaires

Pour tester suffit d'un postman avec un request get ou post avec un body du type :

Exemple :

{
"id": "6f1e94dc-9b0d-4d71-b8f7-2dd632920cf3",
"montant": 50 }

C'est tout le temps le même format de request body (c'est un point à améliorer bien entendu. ), Le montant ne sera juste
non utilisé si pas utile.

## Vidéo en cas d'urgence

Si jamais pour une raison x ou y, vous n'arrivez pas à lancer (intelliJ peut se montrer capricieu, mais normalement pas
de problème)
J'ai mis une vidéo en non répertorié sur youtube qui fait une minie présentation pour vous montrez les fonctionalités.

Lien : https://www.youtube.com/watch?v=Mgbnm6Ab6h4

## Remerciment

Je vous remercie, en attendant impatiemment vos retours. Cordialement, Barbas Guillaume
